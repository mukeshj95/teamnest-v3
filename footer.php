<section class="getStarted">
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="getStarted">
                        <h1 class="bottom">get started now</h1>
                        <div class="column text-center">
                            <p>TeamNest is easy to use, affordable, and free for early adopters. Setup your organisation in minutes, and see TeamNest work wonders for your organisation.</p>
                            <a class="btn btn-primary">Sign Up</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="logo">
                        <img src="assets/img/logo-footer.png" class="img-fluid" alt="Teamnest" title="Teamnest">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-12">
                    <div class="quick_links">
                        <h4>Teamnest Features</h4>
                        <ul>
                            <li>
                                <a href="attendance.php">Attendance</a>
                            </li>
                            <li>
                                <a href="leaves.php">Leaves</a>
                            </li>
                            <li>
                                <a href="expense.php">Expense</a>
                            </li>
                            <li>
                                <a href="helpdesk.php">Helpdesk</a>
                            </li>
                            <li>
                                <a href="engagement.php">Engagement</a>
                            </li>
                            <li>
                                <a href="emplyoee-service.php">Employee Self-Service</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 col-sm-12">
                    <div class="quick_links full">
                        <h4>legal</h4>
                        <ul>
                            <li>
                                <a href="/tnc">Privacy</a>
                            </li>
                            <li>
                                <a href="/privacy">Terms of Service</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2 col-sm-12">
                    <div class="social_icons">
                        <h4>connect</h4>
                        <ul>
                            <li>
                                <a href="#"><img src="assets/img/facebook-icon.png" class="img-fluid" alt="Facebook" title="Facebook"> </a>
                            </li>
                            <li>
                                <a href="#"><img src="assets/img/twitter-icon.png" class="img-fluid" alt="Twitter" title="Twitter"> </a>
                            </li>
                            <li>
                                <a href="#"><img src="assets/img/linkedin-icon.png" class="img-fluid" alt="Linkedin" title="Linkedin"> </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12">
                    <div class="newsletter">
                        <h5>newsletter</h5>
                        <div class="form-group">
                            <!--<label for="newsletter" class="sr-only"></label>-->
                            <input class="form-control" placeholder="enter email">
                            <a class="btn btn-primary">subscribe</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="footer_bottom">
                        <p>©  2018 TESPL. All rights reserved.</p>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</section>
<script src="assets/js/jquery-3.2.1.min.js"></script>
<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/plugins/js/jquery.touchSwipe.min.js"></script>
<script src="assets/js/main.js"></script>
<script>
    $(function() {
        $("#pnProductNavContents").swipe({
            //Generic swipe handler for all directions
            swipe: function (event, direction, distance, duration, fingerCount, fingerData) {
                //$(this).text("You swiped " + direction);
                if(direction == "left"){
                    $(pnAdvancerRight).trigger('click')
                }
                //$('#carousel-left').click();
                else if(direction == "right"){
                    $(pnAdvancerLeft).trigger('click')
                }
            }
        })
    });

</script>
</body>
</html>