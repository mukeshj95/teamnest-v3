<?php @include 'header.php';?>
<section class="pb-0">
    <div class="container">
        <div class="row">
            <div class="col-md-11 col-sm-12 p-0">
                <div class="tabs_main">
                    <ul class="full-wide">
                        <li><a href="attendance.php" id="attendance_active">Attendence</a></li>
                        <li><a href="leaves.php" id="leaves_active">Leaves</a></li>
                        <li><a href="expense.php" id="expense_active">Expenses</a></li>
                        <li><a href="helpdesk.php" id="help_active">Helpdesk</a></li>
                        <li><a href="engagement.php" id="engage_active">Engagement</a></li>
                        <li><a href="emplyoee-service.php" id="emplyoee_active">Employee Self-Service</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
</header>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="attendance">
                    <h1 class="title">Engagement</h1>
                    <p>At TeamNest, we understand the importance of engagement between employees. Thus, we have created a platform where you can connect with your co-workers whenever and wherever you want; even on holidays! Share important updates, recognize achievements, promote healthy competition and increase productivity.</p>
                    <h2>Key Features</h2>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="item">
                                <span><img src="assets/img/engagement-1.png" alt="Engagement Feature" title="Social platform @ work"></span>
                                <p>Social platform @ work</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="item">
                                <span><img src="assets/img/engagement-2.png" alt="Engagement Feature" title="Share updates with your team"></span>
                                <p>Share updates with your team</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="item">
                                <span><img src="assets/img/engagement-3.png" alt="Engagement Feature" title="Encourage Appreciation, Recognition and Rewards"></span>
                                <p>Encourage Appreciation, Recognition and Rewards</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="item">
                                <span><img src="assets/img/engagement-4.png" alt="Engagement Feature" title="Team bonding"></span>
                                <p>Team bonding</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="middle-texts">
                    <div class="m-auto">
                        <img src="assets/img/engagement-app.png" alt="Engagement view" title="Engagement View" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="column timeManage">
                    <h3>Employee engagement platform</h3>
                    <h2>Stay connected, stay informed, always</h2>
                </div>
                <div class="text-center engagment-main">
                    <div class="m-auto">
                        <figure>
                            <img src="assets/img/engagement-main-screen'.png" class="img-fluid" alt="Employee engagement platform" title="Employee engagement platform">
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php @include 'footer.php' ?>
<script>
    $(document).ready(function () {
        $('#engage_active').addClass('active');
    });
</script>
