<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="author" content="Teamnest">
    <!-- Social Share Images -->
    <meta property="og:video:type" content="application/x-shockwave-flash" />
    <meta content="https://teamnest.com/" property="og:url">
    <meta content="IN" name="geo.country" />
    <meta content="index,follow" name="robots"/>
    <meta content="" name="description"/>
    <meta content="" name="keywords"/>
    <link href="" rel="amphtml">
    <meta content="" name="author" />
    <meta content="" name="copyright" />
    <meta content="" name="msvalidate.01" />
    <meta content="" name="google-site-verification" />
    <meta content="" http-equiv="X-UA-Compatible" />
    <meta content="yes" name="mobile-web-app-capable"/>
    <meta content="website" property="og:type">
    <meta content="" name="theme-color">
    <meta content="" name="msapplication-TileColor">
    <meta content="" name="twitter:card">
    <meta content="" name="twitter:app:name:iphone">
    <meta content="" name="twitter:app:id:iphone">
    <meta content="Teamnest" name="twitter:app:name:ipad">
    <meta content="" name="twitter:app:id:ipad">
    <meta content="Teamnest" name="twitter:app:name:googleplay">
    <meta content="" name="twitter:app:id:googleplay"/>
    <meta content="" name="twitter:site">
    <meta content="" name="twitter:title"/>
    <meta content="" name="twitter:description"/>
    <meta content="" name="twitter:creator">
    <meta content="" name="twitter:image:src">
    <meta content="" name="twitter:domain">
    <meta content="" property="og:title">
    <meta content="https://teamnest.com" property="og:url">
    <meta content="" property="og:image">
    <meta content="" property="og:description">
    <title>Teamnest</title>
    <link rel="icon" href="assets/img/favicon.png" type="image/gif" sizes="16x16">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/plugins/css/ionicons.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/custom.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/css/tabs.css">
    <link rel="stylesheet" type="text/css" href="assets/css/response.css">


</head>
<body>
<header>
    <nav class="navbar navbar-default fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="index.php"><img src="assets/img/logo-footer.png" alt="teamnest-logo" title="Teamnest" class="img-fluid"></a>
                <label class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-item" aria-expanded="false" id="menu-toggle">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="ion-navicon"></i>
                </label>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="menu-item">
                <ul class="navbar-lists">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Features</a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-item"><i class="navigation-icons attendance"></i> <a href="attendance.php">  Attendence</a></li>
                            <li class="dropdown-item"><i class="navigation-icons leaves"></i> <a  href="leaves.php">Leaves</a></li>
                            <li class="dropdown-item"><i class="navigation-icons expenses"></i> <a href="expense.php">  Expenses</a></li>
                            <li class="dropdown-item"><i class="navigation-icons help"></i> <a href="helpdesk.php">Helpdesk</a></li>
                            <li class="dropdown-item"><i class="navigation-icons social"></i> <a href="engagement.php">Engagement</a></li>
                            <li class="dropdown-item"><i class="navigation-icons profile"></i> <a href="emplyoee-service.php">Employee                                              Self-Service</a></li>
                            <li class="dropdown-item"><i class="navigation-icons policies"></i> <a href="emplyoee-service.php">Payroll</a></li>
                            <li class="dropdown-item"><i class="navigation-icons reports"></i> <a href="emplyoee-service.php">Compliance</a></li>
                        </ul>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="about-us.php">About</a>
                    </li>
                    <li class="nav-item">
                        <a href="team.php" class="nav-link">Team</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Help</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Sign up</a>
                    </li>

                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
