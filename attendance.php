<?php @include 'header.php'?>

<section class="pb-0">
    <div class="container">
        <div class="row">
            <div class="col-md-11 col-sm-12 p-0">
                <div class="tabs_main">
                    <ul class="full-wide">
                        <li><a href="attendance.php" id="attendance_active">Attendence</a></li>
                        <li><a href="leaves.php" id="leaves_active">Leaves</a></li>
                        <li><a href="expense.php" id="expense_active">Expenses</a></li>
                        <li><a href="helpdesk.php" id="help_active">Helpdesk</a></li>
                        <li><a href="engagement.php" id="engage_active">Engagement</a></li>
                        <li><a href="emplyoee-service.php" id="emplyoee_active">Employee Self-Service</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
</header>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="attendance">
                    <h1 class="title">Attendance</h1>
                    <p>Record your attendance via web punch-in with real time integration. Sync your biometric device with your account. Manage shifts and weekly offs. Organize information about your employees by using our roster management feature.</p>
                    <h2>Key Features</h2>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="item">
                                <span><img src="assets/img/attendance-cloud.png" alt="Attendance" title="Cloud and app-based attendance"></span>
                                <p>Cloud and app-based attendance</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="item">
                                <span><img src="assets/img/attendance-2.png" alt="Attendance" title="Real Timeinsights and analytics"></span>
                                <p>Real Timeinsights and analytics</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="item">
                                <span><img src="assets/img/attendance-3.png" alt="Attendance" title="Configurable work hours"></span>
                                <p>Configurable work hours</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="item">
                                <span><img src="assets/img/attendance-4.png" alt="Attendance" title="Biometric integration"></span>
                                <p>Biometric integration</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="app-view attendance-app">
                    <div class="m-auto">
                        <img src="assets/img/attendance-app.png" alt="App view" title="Attendance View" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="time-location">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="column timeManage">
                    <h3>Attendance management platform</h3>
                    <h2>Time and location tracking anytime, anywhere</h2>
                </div>
                <div class="text-center">
                    <div class="m-auto">
                        <figure>
                            <img src="assets/img/time-location-cover.png" class="img-fluid" alt="Time and location tracking; anytime, anywhere" title="Time and location tracking; anytime, anywhere">
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php @include 'footer.php' ?>


<script>
    $(document).ready(function () {
        $('#attendance_active').addClass('active');
    });

</script>