<?php @include 'header.php';?>
<section class="pb-0">
    <div class="container">
        <div class="row">
            <div class="col-md-11 col-sm-12 p-0">
                <div class="tabs_main">
                    <ul class="full-wide">
                        <li><a href="attendance.php" id="attendance_active">Attendence</a></li>
                        <li><a href="leaves.php" id="leaves_active">Leaves</a></li>
                        <li><a href="expense.php" id="expense_active">Expenses</a></li>
                        <li><a href="helpdesk.php" id="help_active">Helpdesk</a></li>
                        <li><a href="engagement.php" id="engage_active">Engagement</a></li>
                        <li><a href="emplyoee-service.php" id="emplyoee_active">Employee Self-Service</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
</header>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="attendance">
                    <h1 class="title">Helpdesk</h1>
                    <p>The reporting function can help you streamline the process. A centralized issue resolution workflow to help resolve concerns faster. Increase you response efficiency with our simplified framework.</p>
                    <h2>Key Features</h2>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="item">
                                <span><img src="assets/img/helpdesk-1.png" alt="Helpdesk" title="Ticket-based issue tracker"></span>
                                <p>Ticket-based issue tracker</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="item">
                                <span><img src="assets/img/helpdesk-2.png" alt="Helpdesk" title="Single window for all employee"></span>
                                <p>Single window for all employee concerns</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="item">
                                <span><img src="assets/img/helpdesk-3.png" alt="Helpdesk" title="Your support platform"></span>
                                <p>Your support platform wherever you go with our mobile app</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="item">
                                <span><img src="assets/img/helpdesk-4.png" alt="Helpdesk" title="Automatic assignment of tickets"></span>
                                <p>Automatic assignment of tickets to the relevant function heads</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="app-view helpdesk-app">
                    <div class="m-auto">
                        <img src="assets/img/helpdesk-app.png" alt="Helpdesk view" title="Helpdesk View" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="column timeManage">
                    <h3>Ticketing platform</h3>
                    <h2>Address employee concerns, efficiently</h2>
                </div>
                <div class="text-center helpdesk-main">
                    <div class="m-auto">
                        <figure>
                            <img src="assets/img/helpdesk-main-screen.png" class="img-fluid" alt="Ticketing platform" title="Ticketing platform">
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php @include 'footer.php' ?>
<script>
    $(document).ready(function () {
        $('#help_active').addClass('active');
    });
</script>

