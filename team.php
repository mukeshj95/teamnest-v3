<?php @include 'header.php' ?>
<style>
    body{background: url("assets/img/team-1-right.png")top right no-repeat, url("assets/img/team-1-left.png")0% 25% no-repeat,url("assets/img/team-2-right.png") 74% 48% no-repeat, url(assets/img/team-2-left.png)15% 70% no-repeat;}
    @media (max-width: 990px) {
        body{background: none;}
    }
</style>
</header>
<section class="mt-5">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 class="title">Team Members</h1>
            </div>
            <div class="col-md-10 offset-md-1 col-sm-12">
                <div class="row">
                    <div class="col-md-6 col-sm-12 order-md-1">
                        <div class="team-picture">
                            <figure>
                                <img src="assets/img/mahesh-pic.png" class="img-fluid" title="Mahesh Jethani" alt="Mahesh Jethani">
                            </figure>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 order-md-0">
                        <div class="team-details middle-texts">
                            <div class="m-auto">
                                <h2>Mahesh Jethani</h2>
                                <label>ceo</label>
                                <p>Nunc convallis libero eget turpis bibendum efficitur. Fusce eu eros lacus. Sed suscipit dictum ex, ac varius tellus dictum commodo. Sed dictum diam a velit bibendum, non tincidunt nisl tincidunt. Nullam finibus orci quis pellentesque laoreet. In hac habitasse platea dictumst. Aenean eu mi ipsum.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-12 order-md-0">
                        <div class="team-picture">
                            <figure>
                                <img src="assets/img/prashant-pic.png" class="img-fluid" title="Prashant Shah" alt="Prashant Shah">
                            </figure>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 order-md-1">
                        <div class="team-details middle-texts">
                            <div class="m-auto">
                                <h2>Prashant Shah</h2>
                                <label>cmo</label>
                                <p>Nunc convallis libero eget turpis bibendum efficitur. Fusce eu eros lacus. Sed suscipit dictum ex, ac varius tellus dictum commodo. Sed dictum diam a velit bibendum, non tincidunt nisl tincidunt. Nullam finibus orci quis pellentesque laoreet. In hac habitasse platea dictumst. Aenean eu mi ipsum.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 push-md-6 col-sm-12 order-md-1">
                        <div class="team-picture">
                            <figure>
                                <img src="assets/img/saumil-pic.png" class="img-fluid" title="Saumil gandhi" alt="Saumil gandhi">
                            </figure>
                        </div>
                    </div>
                    <div class="col-md-6 pull-md-6 col-sm-12 order-md-0">
                        <div class="team-details middle-texts">
                            <div class="m-auto">
                                <h2>Saumil gandhi</h2>
                                <label>coo</label>
                                <p>Nunc convallis libero eget turpis bibendum efficitur. Fusce eu eros lacus. Sed suscipit dictum ex, ac varius tellus dictum commodo. Sed dictum diam a velit bibendum, non tincidunt nisl tincidunt. Nullam finibus orci quis pellentesque laoreet. In hac habitasse platea dictumst. Aenean eu mi ipsum.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-12 order-md-0">
                        <div class="team-picture">
                            <figure>
                                <img src="assets/img/visruth-pic.png" class="img-fluid" title="Vishrut srivastava" alt="Vishrut srivastava">
                            </figure>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 order-md-1">
                        <div class="team-details middle-texts">
                            <div class="m-auto">
                                <h2>Vishrut srivastava</h2>
                                <label>cto</label>
                                <p>Nunc convallis libero eget turpis bibendum efficitur. Fusce eu eros lacus. Sed suscipit dictum ex, ac varius tellus dictum commodo. Sed dictum diam a velit bibendum, non tincidunt nisl tincidunt. Nullam finibus orci quis pellentesque laoreet. In hac habitasse platea dictumst. Aenean eu mi ipsum.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php @include 'footer.php'?>