<?php @include "header.php"; ?>

</header>
<style>
    body{background: url("assets/img/about-us-right-bg.png")center left no-repeat;background-size: auto;}
</style>
<section class="about-bg">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="middle-texts about-us pt-5 pb-5">
                    <div class="m-auto">
                        <h5>THE STORY BEHIND</h5>
                        <h1 class="title">Simplifying employee experiences for your organisation</h1>
                        <p>Teamnest is a self signup cloud platform to manage your employess and organisation processes quickly and easily. While there are solutionsthat provide various features separately, we believe in creating a product that unites all the features under one roof.</p>
                        <p>This comprehensive offering is tied in by a seamless and intuitive user experience across web, and on our mobile apps. We are a multi-tenant platform. THis means that your organisation's data is secured in your own private location, and not shared on a server with anyone else.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="middle-texts">
                    <div class="m-auto">
                        <figure>
                            <img src="assets/img/about-us-top.png" class="img-fluid" alt="About Teamnest" title="About Teamnest">
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<article>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12 order-md-1">
                <div class="middle-texts about-us">
                    <div class="m-auto">
                        <h2 class="title">Our Vision</h2>
                        <p>We believe that professionals and work forces around the world need a single point platform to run their professional lives easily, intuitively, and securely. From to-dos to compliances to physical workspaces - we will cover everything that you need to ensure you can focus on creating magic at your work place.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 order-md-0">
                <div class="middle-texts">
                    <div class="m-auto">
                        <figure>
                            <img src="assets/img/about-f-1.png" class="img-fluid" alt="Our Vision" title="Our Vision">
                        </figure>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4 offset-md-1 col-sm-12 order-md-0">
                <div class="middle-texts about-us">
                    <div class="m-auto">
                        <h2 class="title">Our Mission</h2>
                        <p>TeamNest came into existence with a single point agenda: to provide world class HR platform to small and medium organisations. We hope to provide every possible convenience and offering - from attendance to payroll to tax filing to investments.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 order-md-1">
                <div class="middle-texts">
                    <div class="m-auto">
                        <figure>
                            <img src="assets/img/about-f-2.png" class="img-fluid" alt="Our Mission" title="Our Mission">
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>
<section class="cardSection">
    <div class="container">
        <div class="card text-center">
            <div class="card-body">
                <div class="card-header">
                    <h1 class="title">What's with the name -Teamnest?</h1>
                </div>
                <p class="main">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam sed semper nulla. Duis ac accumsan nulla, sit amet accumsan velit. Sed commodo pharetra feugiat. Nam vehicula felis vitae orci tempus, et elementum nunc congue. Nunc feugiat ac orci sed ultricies. Morbi congue fermentum purus ac viverra.</p>
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <figure>
                            <img src="assets/img/teamnest-about-step.png" class="img-fluid" alt="Teamnest" title="Teamnest">
                        </figure>
                        <div class="p-4">
                            <h4>Value 1 goes here</h4>
                            <p>Donec pulvinar efficitur ullamcorper. Quisque gravida quam at elit imperdiet accumsan.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <figure>
                            <img src="assets/img/teamnest-about-step2.png" class="img-fluid" alt="Teamnest" title="Teamnest">
                        </figure>
                        <div class="p-4">
                            <h4>Value 1 goes here</h4>
                            <p>Donec pulvinar efficitur ullamcorper. Quisque gravida quam at elit imperdiet accumsan.</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <figure>
                            <img src="assets/img/teamnest-about-step2.png" class="img-fluid" alt="Teamnest" title="Teamnest">
                        </figure>
                        <div class="p-4">
                            <h4>Value 1 goes here</h4>
                            <p>Donec pulvinar efficitur ullamcorper. Quisque gravida quam at elit imperdiet accumsan.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php @include "footer.php"; ?>
