<?php @include 'header.php';?>
<section class="pb-0">
    <div class="container">
        <div class="row">
            <div class="col-md-11 col-sm-12 p-0">
                <div class="tabs_main">
                    <ul class="full-wide">
                        <li><a href="attendance.php" id="attendance_active">Attendence</a></li>
                        <li><a href="leaves.php" id="leaves_active">Leaves</a></li>
                        <li><a href="expense.php" id="expense_active">Expenses</a></li>
                        <li><a href="helpdesk.php" id="help_active">Helpdesk</a></li>
                        <li><a href="engagement.php" id="engage_active">Engagement</a></li>
                        <li><a href="emplyoee-service.php" id="emplyoee_active">Employee Self-Service</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
</header>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="attendance">
                    <h1 class="title">Employee Self-Service</h1>
                    <p>A single interface for allyour employee related information and documents. A great interface for the employee to manage and upgrade their information with a secured workflow for approval for any changes. Highly digitized and automated platform that reduces paperwork for HR Department.</p>
                    <h2>Key Features</h2>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="item">
                                <span><img src="assets/img/emplyoee-service-1.png" alt="Employee Self-Service" title="Manage your profile information"></span>
                                <p>Manage your profile information by academic qualification, medical records, work experience, etc.</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="item">
                                <span><img src="assets/img/emplyoee-service-2.png" alt="Employee Self-Service" title="Download payslips, Form 16, Monthly Tax computation"></span>
                                <p>Download payslips, Form 16, Monthly Tax computation and manage Provisional tax saving declarations.</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="item">
                                <span><img src="assets/img/emplyoee-service-3.png" alt="Employee Self-Service" title="Centralized information repository"></span>
                                <p>Centralized information repository of all employees</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="item">
                                <span><img src="assets/img/emplyoee-service-4.png" alt="Employee Self-Service" title="Upload and manage the Employee Life cycle"></span>
                                <p>Upload and manage the Employee Life cycle related documents</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="app-view no-bg">
                    <div class="m-auto">
                        <img src="assets/img/emplyoee-service-app.png" alt="Helpdesk view" title="Helpdesk View" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="column timeManage">
                    <h3>Self-service platform</h3>
                    <h2>Personal information and document repository, at one place</h2>
                </div>
                <div class="text-center ess-main">
                    <div class="m-auto">
                        <figure>
                            <img src="assets/img/emplyoee-service-main-screen.png" class="img-fluid" alt="Personal information and document repository" title="Personal information and document repository">
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php @include 'footer.php' ?>
<script>
    $(document).ready(function () {
        $('#emplyoee_active').addClass('active');
    });
</script>

