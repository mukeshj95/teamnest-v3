<?php @include 'header.php' ?>
<style>
    body{background: url("assets/img/home-top-right.png")100% 0% no-repeat, url("assets/img/home-top-middle.png")20% 0% no-repeat;background-blend-mode: color-burn;}
</style>
</header>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12 order-md-1">
                <div class="middle-texts sm-response">
                    <div class="m-auto">
                        <figure>
                            <img src="assets/img/home-main-pic.png" class="img-fluid" alt="Teamnest" title="Teamnest Work FLow">
                        </figure>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12 order-md-0">
                <div class="middle-texts home-sec">
                    <div class="m-auto">
                        <h5>Teamnest.com</h5>
                        <h1>Manage your HR with ease</h1>
                        <p>TeamNest is a cloud based service to automate core HR in small and medium sized organisations</p>
                        <a href="#" class="btn btn-primary">request early access</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="text-center">
                    <h1 class="title center">Explore Features</h1>
                </div>
            </div>
            <div class="col-md-12">
                <div class="pn-ProductNav_Wrapper row">
                    <nav id="pnProductNav" class="pn-ProductNav">
                        <div id="pnProductNavContents" class="pn-ProductNav_Contents nav nav-tabs">
                            <a class="nav-link pn-ProductNav_Link active" id="attendence-tab" data-toggle="tab" href="#attendence" role="tab" aria-controls="home" aria-selected="true">
                                <div class="hover-box">
                                    <img src="assets/img/attendance-icon.png" class="img-fluid" alt="Attendance" title="Attendance"/>
                                    <label>Attendance</label>
                                </div>
                            </a>
                            <a class="nav-link pn-ProductNav_Link" id="leaves-tab" data-toggle="tab" href="#leaves" role="tab" aria-controls="leaves" aria-selected="false">
                                <div class="hover-box">
                                    <img src="assets/img/leaves-icon.png" class="img-fluid" alt="Leaves" title="Leaves"/>
                                    <label>Leaves</label>
                                </div>
                            </a>
                            <a class="nav-link pn-ProductNav_Link" id="expenses-tab" data-toggle="tab" href="#expenses" role="tab" aria-controls="expenses" aria-selected="false">
                                <div class="hover-box">
                                    <img src="assets/img/expense-icon.png" class="img-fluid" alt="Expenses" title="Expenses"/>
                                    <label>Expenses</label>
                                </div>
                            </a>
                            <a class="nav-link pn-ProductNav_Link" id="helpdesk-tab" data-toggle="tab" href="#helpdesk" role="tab" aria-controls="helpdesk" aria-selected="false">
                                <div class="hover-box">
                                    <img src="assets/img/helpdesk-icon.png" class="img-fluid" alt="Helpdesk" title="Helpdesk"/>
                                    <label>Helpdesk</label>
                                </div>
                            </a>
                            <a class="nav-link pn-ProductNav_Link" id="engagement-tab" data-toggle="tab" href="#engagement" role="tab" aria-controls="engagement" aria-selected="false">
                                <div class="hover-box">
                                    <img src="assets/img/engagement-icon.png" class="img-fluid" alt="Engagement" title="Engagement"/>
                                    <label>Engagement</label>
                                </div>

                            </a>
                            <a class="nav-link pn-ProductNav_Link" id="SelfService-tab" data-toggle="tab" href="#SelfService" role="tab" aria-controls="SelfService" aria-selected="false">
                                <div class="hover-box">
                                    <img src="assets/img/emplyoee-service-icon.png" class="img-fluid" alt="Employee Self Service" title="Employee Self Service"/>
                                    <label>Employee Self Service</label>
                                </div>
                            </a>
                            <a class="nav-link pn-ProductNav_Link" id="payroll-tab" data-toggle="tab" href="#payroll" role="tab" aria-controls="payroll" aria-selected="false">
                                <div class="hover-box">
                                    <img src="assets/img/payroll-icon.png" class="img-fluid" alt="Payroll Processing" title="Payroll Processing"/>
                                    <label>Payroll Processing</label>
                                </div>
                            </a>
                            <a class="nav-link pn-ProductNav_Link" id="compliance-tab" data-toggle="tab" href="#compliance" role="tab" aria-controls="compliance" aria-selected="false">
                                <div class="hover-box">
                                    <img src="assets/img/complaince-icon.png" class="img-fluid" alt="Compliance" title="Compliance"/>
                                    <label>Compliance</label>
                                </div>
                            </a>
                        </div>
                    </nav>
                    <button id="pnAdvancerLeft" class="pn-Advancer pn-Advancer_Left" type="button">
                        <i class="ion-chevron-left"></i>
                    </button>
                    <button id="pnAdvancerRight" class="pn-Advancer pn-Advancer_Right" type="button">
                        <i class="ion-chevron-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-content homeSection" id="homeSection_tabContent">
        <div class="tab-pane fade show active" id="attendence" role="tabpanel" aria-labelledby="attendence-tab">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12 order-md-0">
                        <div class="middle-texts">
                            <div class="m-auto">
                                <h2>Attendance</h2>
                                <p>Capture attendance from web, mobile or integrate with your existing biometric device. Configure work hours, shifts and weekly offs. Monitor sales and work-from-home employees with real-time GPS tracking.</p>
                                <a href="http://localhost/teamnest-marketing/features.php#attendence">Know More</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12 order-md-1 attendance-bg">
                        <div class="middle-texts p-3">
                            <div class="m-auto">
                                <figure>
                                    <img src="assets/img/attendance-screen.png" class="img-fluid" alt="Attendance Screen" title="Attendance">
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="leaves" role="tabpanel" aria-labelledby="attendence-tab">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12 order-md-0">
                        <div class="middle-texts">
                            <div class="m-auto">
                                <h2>Leaves</h2>
                                <p>Capture attendance from web, mobile or integrate with your existing biometric device. Configure work hours, shifts and weekly offs. Monitor sales and work-from-home employees with real-time GPS tracking.</p>
                                <a href="http://localhost/teamnest-marketing/features.php#leaves">Know More</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12 order-md-1 leaves-bg">
                        <div class="middle-texts p-3">
                            <div class="m-auto">
                                <figure>
                                    <img src="assets/img/leaves-screen.png" class="img-fluid" alt="Leaves Screen" title="Leaves">
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="expenses" role="tabpanel" aria-labelledby="attendence-tab">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12 order-md-0">
                        <div class="middle-texts">
                            <div class="m-auto">
                                <h2>Expenses</h2>
                                <p>Capture attendance from web, mobile or integrate with your existing biometric device. Configure work hours, shifts and weekly offs. Monitor sales and work-from-home employees with real-time GPS tracking.</p>
                                <a href="http://localhost/teamnest-marketing/features.php#expenses">Know More</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12 order-md-1 expense-bg">
                        <div class="middle-texts">
                            <div class="m-auto">
                                <figure>
                                    <img src="assets/img/expense-screen.png" class="img-fluid" alt="Expense Screen" title="Expense">
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="helpdesk" role="tabpanel" aria-labelledby="attendence-tab">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12 order-md-0">
                        <div class="middle-texts">
                            <div class="m-auto">
                                <h2>Helpdesk</h2>
                                <p>Capture attendance from web, mobile or integrate with your existing biometric device. Configure work hours, shifts and weekly offs. Monitor sales and work-from-home employees with real-time GPS tracking.</p>
                                <a href="http://localhost/teamnest-marketing/features.php#helpdesk">Know More</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12 order-md-1 helpdesk-bg">
                        <div class="middle-texts">
                            <div class="m-auto">
                                <figure>
                                    <img src="assets/img/helpdesk-screen.png" class="img-fluid" alt="Helpdesk Screen" title="Helpdesk">
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="engagement" role="tabpanel" aria-labelledby="attendence-tab">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12 order-md-0">
                        <div class="middle-texts">
                            <div class="m-auto">
                                <h2>Engagement</h2>
                                <p>Capture attendance from web, mobile or integrate with your existing biometric device. Configure work hours, shifts and weekly offs. Monitor sales and work-from-home employees with real-time GPS tracking.</p>
                                <a href="http://localhost/teamnest-marketing/features.php#engagement">Know More</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12 order-md-1 helpdesk-bg">
                        <div class="middle-texts">
                            <div class="m-auto">
                                <figure>
                                    <img src="assets/img/engagement-screen.png" class="img-fluid" alt="Engagement Screen" title="Engagement">
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="SelfService" role="tabpanel" aria-labelledby="attendence-tab">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12 order-md-0">
                        <div class="middle-texts">
                            <div class="m-auto">
                                <h2>Employee Self Service</h2>
                                <p>Capture attendance from web, mobile or integrate with your existing biometric device. Configure work hours, shifts and weekly offs. Monitor sales and work-from-home employees with real-time GPS tracking.</p>
                                <a href="http://localhost/teamnest-marketing/features.php#selfService">Know More</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12 order-md-1 attendance-bg">
                        <div class="middle-texts">
                            <div class="m-auto">
                                <figure>
                                    <img src="assets/img/emplyoee-service-screen.png" class="img-fluid" alt="Emplyoee-service Screen" title="Emplyoee-service">
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="payroll" role="tabpanel" aria-labelledby="attendence-tab">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12 order-md-0">
                        <div class="middle-texts">
                            <div class="m-auto">
                                <h2>Payroll</h2>
                                <p>Capture attendance from web, mobile or integrate with your existing biometric device. Configure work hours, shifts and weekly offs. Monitor sales and work-from-home employees with real-time GPS tracking.</p>
                                <a href="#">Know More</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12 order-md-1">
                        <figure>
                            <img src="assets/img/attendance-screen.png" class="img-fluid" alt="Attendance Screen" title="Attendance Screen">
                        </figure>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="compliance" role="tabpanel" aria-labelledby="attendence-tab">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12 order-md-0">
                        <div class="middle-texts">
                            <div class="m-auto">
                                <h2>Compliance</h2>
                                <p>Capture attendance from web, mobile or integrate with your existing biometric device. Configure work hours, shifts and weekly offs. Monitor sales and work-from-home employees with real-time GPS tracking.</p>
                                <a href="#">Know More</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12 order-md-1">
                        <figure>
                            <img src="assets/img/attendance-screen.png" class="img-fluid" alt="Attendance Screen" title="Attendance Screen">
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="whyTeam-sec">
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1 col-sm-12">
                <div class="text-center whyTeam">
                    <h1 class="title center">Why Teamnest?</h1>
                    <p>Teamnest is a self signup cloud platform in which you can maintain the data of all your employees, which is absolutely easy to set up. While there are websites out there who provide various features separately, we believe in creating a product that unites all the features under one roof, which as a result refrains you from having separate entities for separate work - easy and comfortable. Teamnest is highly secured and conveniently flexible at the same time.</p>
                </div>
                <div class="row">
                    <div class="col-md-5 col-sm-12 order-md-1">
                        <div class="middle-texts whyTeam">
                            <div class="m-auto">
                                <h3>Beautifully simple interface</h3>
                                <p>Intuitive and easy to use platform with a seamless experience on web and mobile.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-12 m-auto order-md-0">
                        <figure>
                            <img src="assets/img/whyTeam-1.png" alt="Interface" title="Beautifully simple interface" class="img-fluid">
                        </figure>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5 col-sm-12 order-md-0">
                        <div class="middle-texts whyTeam">
                            <div class="m-auto">
                                <h3>Highly configurable</h3>
                                <p>Powerful features to customize the platform to match your organisation's structure and policies.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-12 order-md-1 m-auto">
                        <figure>
                            <img src="assets/img/whyTeam-2.png" alt="Interface" title="Beautifully simple interface" class="img-fluid">
                        </figure>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5 col-sm-12 order-md-1">
                        <div class="middle-texts whyTeam">
                            <div class="m-auto">
                                <h3>Industry standard security</h3>
                                <p>Each organisation has it's own tenant to save their information.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-12 m-auto order-md-0">
                        <figure>
                            <img src="assets/img/whyTeam-3.png" alt="Interface" title="Beautifully simple interface" class="img-fluid">
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<?php @include 'footer.php' ?>

<script>
    /*    $('.btnNext').click(function(){
            $('.nav-tabs > .active').next('li').find('a').trigger('click');
        });

        $('.btnPrevious').click(function(){
            $('.nav-tabs > .active').prev('li').find('a').trigger('click');
        });*/


</script>
