<?php @include 'header.php'; ?>
<section class="pb-0">
    <div class="container">
        <div class="row">
            <div class="col-md-11 col-sm-12 p-0">
                <div class="tabs_main">
                    <ul class="full-wide">
                        <li><a href="attendance.php" id="attendance_active">Attendence</a></li>
                        <li><a href="leaves.php" id="leaves_active">Leaves</a></li>
                        <li><a href="expense.php" id="expense_active">Expenses</a></li>
                        <li><a href="helpdesk.php" id="help_active">Helpdesk</a></li>
                        <li><a href="engagement.php" id="engage_active">Engagement</a></li>
                        <li><a href="emplyoee-service.php" id="emplyoee_active">Employee Self-Service</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
</header>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="attendance leaves">
                    <h1 class="title">Leaves</h1>
                    <p>Creating flexible leave policies based on Calendar or Financial year. Easy interface to apply and approve leave requests - web or mobile. Encash accumulated leaves at the time of working based on company's leave encashment policy. Seamless transfer to the New Year with carry forward leaves and opening balances as per company policy.</p>
                    <h2>Key Features</h2>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="item">
                                <span><img src="assets/img/leaves-1.png" alt="Leaves Features" title="Configurable leave policies"></span>
                                <p>Configurable leave policies</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="item">
                                <span><img src="assets/img/leaves-2.png" alt="Leaves Features" title="Easy Leave approval"></span>
                                <p>Easy Leave approval workflow</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="item">
                                <span><img src="assets/img/leaves-3.png" alt="Leaves Features" title="Customized holiday"></span>
                                <p>Customized holiday calendars</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="item">
                                <span><img src="assets/img/leaves-4.png" alt="Leaves Features" title="Mobile app"></span>
                                <p>Mobile app for leave applications and approvals</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="app-view">
                    <div class="m-auto">
                        <img src="assets/img/leaves-app.png" alt="Leaves view" title="Leaves View" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="leavesPlatform">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="column timeManage">
                    <h3>Leave Management Platform</h3>
                    <h2>Applying and managing leaves, made easy</h2>
                </div>
                <div class="text-center">
                    <div class="m-auto">
                        <figure>
                            <img src="assets/img/leaves-main-screen.png" class="img-fluid" alt="Leave Management Platform" title="Leave Management Platform">
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php @include 'footer.php' ?>
<script>
    $(document).ready(function () {
        $('#leaves_active').addClass('active');
    });
</script>
