<?php @include 'header.php'; ?>
<section class="pb-0">
    <div class="container">
        <div class="row">
            <div class="col-md-11 col-sm-12 p-0">
                <div class="tabs_main">
                    <ul class="full-wide">
                        <li><a href="attendance.php" id="attendance_active">Attendence</a></li>
                        <li><a href="leaves.php" id="leaves_active">Leaves</a></li>
                        <li><a href="expense.php" id="expense_active">Expenses</a></li>
                        <li><a href="helpdesk.php" id="help_active">Helpdesk</a></li>
                        <li><a href="engagement.php" id="engage_active">Engagement</a></li>
                        <li><a href="emplyoee-service.php" id="emplyoee_active">Employee Self-Service</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
</header>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="attendance leaves">
                    <h1 class="title">Expenses</h1>
                    <p>Keep a track of your expenses, capture a receipt of it on your phone and upload it as a supporting from anywhere with the Mobile App. Get an approval on your expense by using our simpleworkflow.Easy Interface to set up the Mileage rate for 2/4 wheelers.</p>
                    <h2>Key Features</h2>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="item">
                                <span><img src="assets/img/expense-1.png" alt="expense Features" title="App-based expense"></span>
                                <p>App-based expense tracking for reimbursements</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="item">
                                <span><img src="assets/img/expense-2.png" alt="expense Features" title="Customized expense approval"></span>
                                <p>Customized expense approval workflows</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="item">
                                <span><img src="assets/img/expense-3.png" alt="expense Features" title="Track the GST component"></span>
                                <p>Track the GST component for the set-off</p>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <div class="item">
                                <span><img src="assets/img/expense-4.png" alt="expense Features" title="Categorize by Travel, Business Promotion"></span>
                                <p>Categorize by Travel, Business Promotion or any other desired Expense Head</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="app-view">
                    <div class="m-auto">
                        <img src="assets/img/expense-app.png" alt="Expense view" title="Expense View" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="column timeManage">
                    <h3>Expense management platform</h3>
                    <h2>Expense tracking and reimbursements, at your fingertips</h2>
                </div>
                <div class="text-center feature-bg">
                    <div class="m-auto">
                        <figure>
                            <img src="assets/img/expense-main.png" class="img-fluid" alt="Expense Management Platform" title="Expense Management Platform">
                        </figure>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php @include 'footer.php' ?>
<script>
    $(document).ready(function () {
        $('#expense_active').addClass('active');
    });
</script>

